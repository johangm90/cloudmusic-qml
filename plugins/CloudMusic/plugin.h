#ifndef CLOUDMUSICPLUGIN_H
#define CLOUDMUSICPLUGIN_H

#include <QQmlExtensionPlugin>

class CloudMusicPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
